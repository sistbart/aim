package de.sbarth.aim.viewmodel;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import de.sbarth.aim.model.FileComparator;
import de.sbarth.aim.model.ImageItem;
import de.sbarth.aim.MainApplication;
import de.sbarth.aim.ui.UiUtils;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.concurrent.Task;
import javafx.stage.DirectoryChooser;

public class IconOrganisatorViewModel {
    private final BooleanProperty runningProperty = new SimpleBooleanProperty(false);
    private final BooleanProperty copyAllResolutions = new SimpleBooleanProperty(UiUtils.getCopyAllResolutions());
    private final DoubleProperty progressProperty = new SimpleDoubleProperty(0.0);
    private final ObjectProperty<FilteredList<ImageItem>> imageItemsProperty = new SimpleObjectProperty(new FilteredList<>(FXCollections.observableArrayList()));
    private final StringProperty searchValuesProperty = new SimpleStringProperty("");
    private final StringProperty allowedDpiType = new SimpleStringProperty();

    public IconOrganisatorViewModel() {
        searchValuesProperty.addListener((observable, oldValue, newValue) -> {
            imageItemsProperty.get().setPredicate(this::isMatchingSearchCriteria);
        });
        allowedDpiType.addListener((observable, oldValue, newValue) -> {
            imageItemsProperty.get().setPredicate(this::isMatchingSearchCriteria);
        });
        copyAllResolutions.addListener((observable, oldValue, newValue) -> {
            UiUtils.setCopyAllResolutions(newValue);
        });
    }

    public void copyImages() {
        imageItemsProperty.get().filtered(imageItem -> imageItem.isSelected()).forEach(imageItem -> {
            try {
                final File parentFile = imageItem.getFile().getParentFile();
                final String parentPath = parentFile.getParent();

                if (parentFile.getName().startsWith("drawable-") && copyAllResolutions.get()) {
                    final String pathPattern = parentPath + "/drawable-%s/" + imageItem.getFile().getName();

                    final File fileMdpi = new File(String.format(pathPattern, "mdpi"));
                    final File fileHdpi = new File(String.format(pathPattern, "hdpi"));
                    final File fileXhdpi = new File(String.format(pathPattern, "xhdpi"));
                    final File fileXxhdpi = new File(String.format(pathPattern, "xxhdpi"));
                    final File fileXxxhdpi = new File(String.format(pathPattern, "xxxhdpi"));

                    copyFiles(fileMdpi, fileHdpi, fileXhdpi, fileXxhdpi, fileXxxhdpi);
                } else {
                    copyFiles(imageItem.getFile());
                }
            } catch (final IOException e) {
            } finally {
                imageItem.setSelected(false);
            }
        });
    }

    private void copyFiles(final File... files) throws IOException {
        for (final File file : files) {
            String parentFolder = file.getParentFile().getName();
            if (!parentFolder.startsWith("drawable-")) {
                parentFolder = "drawable";
            }

            final File destinationFolder = new File(UiUtils.getSelectedProjectPath() + "/" + parentFolder + "/");
            if (!destinationFolder.exists()) {
                destinationFolder.mkdirs();
            }

            final Path source = Paths.get(file.getPath());
            final Path destination = Paths.get(destinationFolder.getPath() + "/" + file.getName());
            Files.copy(source, destination);
        }
    }

    public void loadImagesAsync(final String rootPath) {
        final LoadImagesTask task = new LoadImagesTask(rootPath);
        task.setOnSucceeded(event -> {
            final ObservableList<ImageItem> imageItems = (ObservableList<ImageItem>) event.getSource().getValue();
            imageItemsProperty.setValue(new FilteredList<>(imageItems, this::isMatchingSearchCriteria));
        });
        progressProperty.bind(task.progressProperty());
        runningProperty.bind(task.runningProperty());
        new Thread(task).start();
    }

    public void addProjectPath() {
        final DirectoryChooser directoryChooser = new DirectoryChooser();
        directoryChooser.setTitle("Open Drawable folder of Android Project");
        final File initialDirectory = new File(System.getProperty("user.home"));
        directoryChooser.setInitialDirectory(initialDirectory.exists() ? initialDirectory : new File(System.getProperty("user.home")));

        final File file = directoryChooser.showDialog(null);
        if (file != null) {
            UiUtils.addProjectPath(file.getPath());
            UiUtils.setSelectedProjectPath(file.getPath());
        }
    }

    private boolean isMatchingSearchCriteria(final ImageItem imageItem) {
        final String[] searchValues = searchValuesProperty.get().split(";");
        final String filePath = imageItem.getFilePath();
        final String parentFolderName = imageItem.getFile().getParentFile().getName();

        for (final String searchValue : searchValues) {
            if (!filePath.contains(searchValue)) {
                return false;
            }
        }

        if ("all".equals(allowedDpiType.get())) {
            return true;
        }

        if ("others".equals(allowedDpiType.get())) {
            return !filePath.contains("drawable");
        }

        return ("drawable-" + allowedDpiType.get()).equals(parentFolderName);
    }

    public ObjectProperty<FilteredList<ImageItem>> imageItemsProperty() {
        return imageItemsProperty;
    }

    public DoubleProperty progressProperty() {
        return progressProperty;
    }

    public BooleanProperty runningProperty() {
        return runningProperty;
    }

    public StringProperty searchValueProperty() {
        return searchValuesProperty;
    }

    public StringProperty allowedDpiTypeProperty() {
        return allowedDpiType;
    }

    public BooleanProperty copyAllResolutions() {
        return copyAllResolutions;
    }

    private static final class LoadImagesTask extends Task<ObservableList<ImageItem>> {
        private final String rootPath;
        private long progress;

        private LoadImagesTask(final String rootPath) {
            this.rootPath = rootPath;
        }

        @Override
        protected ObservableList<ImageItem> call() throws Exception {
            final File rootFile = new File(rootPath);

            final List<File> images = getAllImages(rootFile);
            final List<ImageItem> imageItems = images.stream()
                    .sorted(new FileComparator())                                  // order them by path
                    .map(image -> new ImageItem(image))                            // map them into ImageItems
                    .collect(Collectors.toCollection(ArrayList::new));             // collect into list

            return FXCollections.observableArrayList(imageItems);
        }

        private List<File> getAllImages(final File root) {
            final List<File> images = new ArrayList<File>();
            for (final File file : root.listFiles()) {
                if (file.isDirectory()) {
                    images.addAll(getAllImages(file));
                } else if (isImage(file)) {
                    images.add(file);
                }
            }

            progress += images.size();
            updateProgress(progress, MainApplication.MAX_AVAILABLE_IMAGES);

            return images;
        }

        private boolean isImage(final File file) {
            return file.getName().endsWith(".png");
        }
    }
}
